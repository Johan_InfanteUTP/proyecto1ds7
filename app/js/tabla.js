$(document).ready(function() {
var idPregunta, opcion;
var numeral=1;
opcion = 4;
    
tablaPregunta = $('#tablaPregunta').DataTable({  
    "ajax":{            
        "url": "../../db/collector/crud.php", 
        "method": 'POST', //usamos el metodo POST
        "data":{opcion:opcion}, //enviamos opcion 4 para que haga un SELECT
        "dataSrc":""
    },
    "columns":[
        {"data": "enc_id"},
        {"data": "enc_pregunta"},
        {"data": "enc_tipo"},
        {"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>edit</i></button><button class='btn btn-danger btn-sm btnBorrar'><i class='material-icons'>delete</i></button></div></div>"}
    ]
});     

var fila; //captura la fila, para editar o eliminar
//submit para el Alta y Actualización
$('#formUsuarios').submit(function(e){                         
    e.preventDefault(); //evita el comportambiento normal del submit, es decir, recarga total de la página
    pregunta = $.trim($('#pregunta').val());    
    TipoPregunta = $.trim($('#TipoPregunta').val());
    arrayrespuestas =   JSON.stringify($("input[name='respuestas[]']").map(function(){return $(this).val();}).get());
    alert (arrayrespuestas); 

        $.ajax({
          url: "../../db/collector/crud.php",
          type: "POST",
          datatype:"aplication/json",    
          data:  {idPregunta:idPregunta,pregunta:pregunta,TipoPregunta:TipoPregunta,arrayrespuestas:arrayrespuestas,opcion:opcion},    
          success: function(data) {
            tablaPregunta.ajax.reload(null, false);
           }
        });			        
    $('#modalCRUD').modal('hide');											     			
});
        
 

//para limpiar los campos antes de dar de Alta una Persona
$("#btnNuevo").click(function(){
    opcion = 1; //alta           
    idPregunta=null
    $("#formUsuarios").trigger("reset");
    $("#Respuesta").css( "display", "none");
    $(".modal-header").css( "background-color", "#f2387f");
    $(".modal-header").css( "color", "white" );
    $(".modal-title").text("Ingrese Pregunta Nueva");
    $('#modalCRUD').modal('show');	    
});

$("#TipoPregunta").change(function(){
    if($(this).val()=="1"){
        document.getElementById("Respuesta").style.display = "none";  
      }
       if($(this).val()=="2"){
        document.getElementById("Respuesta").style.display = "inline-block";
      }
       if($(this).val()=="3"){
        document.getElementById("Respuesta").style.display = "inline-block";
      }
});

$(".btnadd").click(function(){
    numeral=numeral+1;
    $("#Respuesta").append("<div class='row'  id='"+numeral+"'><div class='col-lg-9'><div class='form-group'><input type='text' class='form-control' name='respuestas[]'></div></div><div class='col-lg-3'><div class='form-group'><button class='btn btn-danger  btndelete'><i class='fa fa-trash' aria-hidden='true'></i></i></button></div></div></div>");
}); 


$(document).on("click", ".btndelete", function(){
    $("#"+numeral).remove();
    numeral=numeral-1;
 });


//Editar        
$(document).on("click", ".btnEditar", function(){		        
    opcion = 2;//editar
    fila = $(this).closest("tr");	        
    idPregunta = parseInt(fila.find('td:eq(0)').text()); //capturo el ID		            
    pregunta = fila.find('td:eq(1)').text();
    TipoPregunta=fila.find('td:eq(2)').text();
    $("#pregunta").val(pregunta);
    $("#TipoPregunta").val(TipoPregunta);
    $(".modal-header").css("background-color", "#f2387f");
    $(".modal-header").css("color", "white" );
    $(".modal-title").text("Editar Pregunta");		
    $('#modalCRUD').modal('show');		   
});

//Borrar
$(document).on("click", ".btnBorrar", function(){
    fila = $(this);           
    idPregunta = parseInt($(this).closest('tr').find('td:eq(0)').text()) ;		
    opcion = 3; //eliminar        
    var respuesta = confirm("¿Está seguro de borrar el registro "+idPregunta+"?");                
    if (respuesta) {            
        $.ajax({
          url: "../../db/collector/crud.php",
          type: "POST",
          datatype:"json",    
          data:  {opcion:opcion, idPregunta:idPregunta},    
          success: function() {
            tablaPregunta.row(fila.parents('tr')).remove().draw();                  
           }
        });	
    }
 });
     
});    
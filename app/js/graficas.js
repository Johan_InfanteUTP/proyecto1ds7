$(document).ready(function () {
    $.ajax({
        type: 'POST',
        url: "../../db/collector/col_highChartsProvincia.php",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function (result) {
            var options = {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    renderTo: 'piechartProvincias'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> <br> Cantidad: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: true
                    }
                },
                series: [result]
            };
            var chart = new Highcharts.Chart(options);
        }
    });
});

$(document).ready(function () {
    $.ajax({
        type: 'POST',
        url: "../../db/collector/col_highChartsSalario.php",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function (result) {
            var options = {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    renderTo: 'piechartSalario'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> <br> Cantidad: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: true
                    }
                },
                series: [result]
            };
            var chart = new Highcharts.Chart(options);
        }
    });
});

$(document).ready(function () {
    $.ajax({
        type: 'POST',
        url: "../../db/collector/col_highChartsEdad.php",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function (result) {
            var options = {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    renderTo: 'piechartEdad'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}% </b> <br> Cantidad: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: true
                    }
                },
                series: [result]
            };
            var chart = new Highcharts.Chart(options);
        }
    });
});

$(document).ready(function () {
    $.ajax({
        type: 'POST',
        url: "../../db/collector/col_highChartsSexo.php",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function (result) {
            var options = {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    renderTo: 'piechartSexo'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> <br>Cantidad: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: true
                    }
                },
                series: [result]
            };
            var chart = new Highcharts.Chart(options);
        }
    });
});

$(document).ready(function () {
    $.ajax({
        type: 'POST',
        url: "../../db/collector/col_highChartsTotal.php",
        dataType: "json",
        contentType: 'application/json',
        async: false,
        success: function (result) {
            var options = {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false,
                    renderTo: 'totalDunus'
                },
                title: {
                    text: 'Encuestas <br> Realizadas',
                    align: 'center',
                    verticalAlign: 'middle',
                    y: 60
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            style: {
                                fontWeight: 'bold',
                                color: 'white'
                            }
                        },
                        startAngle: -90,
                        endAngle: 90,
                        center: ['50%', '75%'],
                        size: '110%'
                    }
                },
                series: [result]
          };
            var chart = new Highcharts.Chart(options);
        }
    });
});


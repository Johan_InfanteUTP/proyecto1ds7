<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Proyecto1 - Encuesta</title>
    <link rel="stylesheet" href="../../app/css/encuesta.css">
</head>
<body>

<?php
include("../../db/config/conexion.php");
include("../../db/collector/col_Usuario.php");

$nombre=$_REQUEST['nombre'];
$edad=$_REQUEST['edad'];
$provincia=$_REQUEST['provincia'];
$salario=$_REQUEST['salario'];
$sexo=$_REQUEST['sexo'];

$cnn= conectar();
UsuInsertar($cnn,$nombre,$edad,$salario,$provincia,$sexo);
desconectar($cnn);

?>
    <div class="container">
 
      <main>

        <div id="survey-form">

            <div class="form">
                <!--first sections-->
                <label id="name-label" for="name">Muchas Gracias por llenar la prueba</label>
            </div>
            <div class="form ">
                <a href="../../index.html" ><button class="submit-btn">salir</button></a>
            </div>
        </div>
      </main>
    </div>


</body>
</html>
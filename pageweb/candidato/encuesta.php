<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Proyecto1 - Encuesta</title>
    <link rel="stylesheet" href="../../app/css/encuesta.css">
</head>
<body>

<?php
include("../../db/config/conexion.php");
include("../../db/collector/col_EncuestaView.php");

$cnn= conectar();
$Preguntas=EncPreguntas($cnn);
$respuestas=EncRespuestas($cnn);
$edades=EncEdades($cnn);
$provincias=EncProvincias($cnn);
$salarios=EncSalarios($cnn);
$sexos=EncSexo($cnn);
desconectar($cnn);

$max=mysqli_num_rows($Preguntas);


if ($max>0)
{
?>
    <div class="container">
      <header class="header center">
        <h1 id="title">Mi primer formulario de encuesta</h1>
        <p id="description">Gracias por tomarse el tiempo de ayudarme en mi especialización</p>
      </header>
      <main>

        <form id="survey-form" action="gracias.php" method="POST" >

          <div class="form">
            <!--first sections-->
            <label id="name-label" for="name">Nombre: </label>
            <input id="name" name="nombre" placeholder="Ingresa tu nombre" type="text" class="text" required>

            <label for="" class="col-form-label" >Elija su sexo</label>
            <select  name="sexo" class="form-control">
              <?php foreach($sexos as $sexo): ?>
              <option value="<?php echo $sexo['sex_id']?>"><?php echo $sexo['sex_nombre']?></option>
              <?php endforeach ?>
            </select>

            <label for="" class="col-form-label" >Elija su rango de edad</label>
            <select  name="edad" class="form-control">
              <?php foreach($edades as $edad): ?>
              <option value="<?php echo $edad['ed_id']?>"><?php echo $edad['ed_rango']?></option>
              <?php endforeach ?>
            </select>

            <label for="" class="col-form-label" >Elija su provincia de nacimiento</label>
            <select  name="provincia" class="form-control">
              <?php foreach($provincias as $provincia): ?>
              <option value="<?php echo $provincia['pro_id']?>"><?php echo $provincia['pro_nombre']?></option>
              <?php endforeach ?>
            </select>

            <label for="" class="col-form-label" >Elija su rango de salario</label>
            <select name="salario" class="form-control">
              <?php foreach($salarios as $salario): ?>
              <option value="<?php echo $salario['sal_id']?>"><?php echo $salario['sal_rango']?></option>
              <?php endforeach ?>
            </select>

    </div>
<?php
    foreach($Preguntas as $Pregunta):
      if($Pregunta['enc_tipo']==1)
      {
?>  
      <div class="form ">
        <!--Radio Binario -->
        <label><?php echo $Pregunta['enc_pregunta'] ?></label>
        <label>
          <input name="<?php echo 'Pregunta'.$Pregunta['enc_id'] ?>" value="1" type="radio" class="radio">
          Si
        </label>
        <label>
          <input name="<?php echo 'Pregunta'.$Pregunta['enc_id'] ?>" value="2" type="radio" class="radio ">
          No
        </label>
      </div>
<?php
      }elseif($Pregunta['enc_tipo']==2){
?>
      <div class="form ">
        <!--Radio-->
        <label><?php echo $Pregunta['enc_pregunta']?></label>
          <?php foreach($respuestas as $respuesta): 
            if($respuesta['resp_idPregunta']==$Pregunta['enc_id']){?>
            <label>
              <input name="<?php echo 'Pregunta'.$Pregunta['enc_id'] ?>" value="<?php echo $respuesta['resp_respuesta']?>" type="radio" class="radio">
              <?php echo $respuesta['resp_respuesta']?>
            </label>
            <?php }endforeach?>
      </div>
<?php
      }elseif($Pregunta['enc_tipo']==3){
?>
      <div class="form">
        <!--Checkboxes-->
        <label><?php echo $Pregunta['enc_pregunta']?></label>
        <div class="capitalize">
        <?php foreach($respuestas as $respuesta): 
            if($respuesta['resp_idPregunta']==$Pregunta['enc_id']){?>
              <label>
                <input name="<?php echo 'Pregunta'.$Pregunta['enc_id'] ?>" value="<?php echo $respuesta['resp_respuesta']?>" type="checkbox" class="checkbox">
                <?php echo $respuesta['resp_respuesta']?>
            </label>
            <?php }endforeach?>
        </div>
      </div>

<?php
    }
  endforeach
?>
      <div class="form">
        <!--Text area-->
        <label>Algún comentario o sugerencia?</label>
        <textarea name="comment" class="textarea" id="comments" placeholder="Ingresa tu comentario"></textarea>
      </div>

      <div class="form ">
        <button type="submit" id="submit" class="submit-btn">Enviar</button>
      </div>

    </form>
  </main>
</div>
<?php
}
else
{
?>

<main id="survey-form">
  <div class="form center">
    <label> Disculpe las molestia, estamos teniendo inconvenientes de conexion. <br> Porfavor actualice la pagina ó notifique a soporte tecnico</label>
  </div>
</main>

<?php
}

?>
      <!-- <div class="form">
        <label for="dropdown">Lenguajes de programación más usados en el mundo: </label>
        <select name="role" id="dropdown" class="form-control capitalize" required>
          <option disabled selected value>Elige tu favorito</option>
          <option value="php">php</option>
          <option value="javascript">javascript</option>
          <option value="java">java</option>
          <option value="python">python</option>
          <option value="ruby">ruby</option>
          <option value="c++">c++</option>
        </select>
      </div> -->


</body>
</html>
<?php
	include_once '../config/conexionPDO.php';
    include("HighCharts/HighCharts.php");
    
	$objeto = new Conexion();
	$conexion = $objeto->Conectar();

	$consulta = "call SP_Dashboard_GetGraficaTotal";			
    $resultado = $conexion->prepare($consulta);
	$resultado->execute(); 

	$chart = new HighChartsPie();

    $chart->name = "Cantidad";
    $chart ->innerSize = '45%';
    $chart ->type = 'pie';

	$chart->colorByPoint = true;
	//Inicializar datos
	$chart->data = array();

	while ($data1=$resultado->fetch(PDO::FETCH_ASSOC)) {
			$chart_column = new Data();
			$chart_column->name = 'Pruebas Realizadas';
			$chart_column->y = intval($data1['cantidad']);
			$chart_column->color = "#".dechex(rand(0x000000, 0xFFFFFF));
			array_push($chart->data, $chart_column); //col1
	  }

	echo json_encode($chart);
?>

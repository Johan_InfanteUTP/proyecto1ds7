<?php
	include_once '../config/conexionPDO.php';
	include("HighCharts/HighCharts.php");

	$objeto = new Conexion();
	$conexion = $objeto->Conectar();

	$consulta = "call SP_Dashboard_GetGraficaSexo";			
    $resultado = $conexion->prepare($consulta);
	$resultado->execute(); 

	$chart = new HighChartsPie();

	$chart->name = "Porcentaje";
	$chart->colorByPoint = true;
	//Inicializar datos
	$chart->data = array();

	while ($data1=$resultado->fetch(PDO::FETCH_ASSOC)) {
			$chart_column = new Data();
			$chart_column->name = $data1['nombre'];
			$chart_column->y = intval($data1['cantidad']);
			$chart_column->sliced = false;
			$chart_column->selected = false;
			$chart_column->color = "#".dechex(rand(0x000000, 0xFFFFFF));
			array_push($chart->data, $chart_column); //col1
	  }

	echo json_encode($chart);
?>